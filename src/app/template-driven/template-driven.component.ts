import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-template-driven',
  templateUrl: './template-driven.component.html',
  styleUrls: ['./template-driven.component.scss']
})
export class TemplateDrivenComponent implements OnInit {
  form1: NgForm;

  form2: string = "";
  form2_1: string;
  form2_2: string;
  form3: string;
  form3_1: string;
  form4: string;
  form4_1: string;

  constructor(


  ) { }

  ngOnInit(): void {
  }

}
